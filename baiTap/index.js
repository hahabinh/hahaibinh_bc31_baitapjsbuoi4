// Bài 1

document.getElementById("btn-sap-xep").addEventListener
("click",function () {

    var soThu1Value = document.getElementById("txt-so-thu-1").value*1;
    var soThu2Value = document.getElementById("txt-so-thu-2").value*1;
    var soThu3Value = document.getElementById("txt-so-thu-3").value*1;

    if(soThu1Value>soThu2Value){
        if (soThu2Value>soThu3Value) {
            alert("Thứ tự :" + soThu3Value + soThu2Value + soThu1Value);
        }else if(soThu2Value<soThu3Value && soThu1Value>soThu3Value){
            alert("Thứ tự :" + soThu2Value + soThu3Value + soThu1Value);
        }else{
            alert("Thứ tự :" +soThu2Value +soThu1Value +soThu3Value)
        }
    }else{
        if(soThu2Value<soThu3Value){
            alert("Thứ tự :" + soThu1Value +soThu2Value +soThu3Value );
        }else if(soThu2Value>soThu3Value && soThu1Value<soThu3Value){
            alert("Thứ tự :" +soThu1Value +soThu3Value +soThu2Value);
        }else{
            alert("Thứ tự :" +soThu3Value +soThu1Value +soThu2Value)
        }
    }
});

// Bài 2

document.getElementById("btn-loi-chao").addEventListener
("click",function(){
    var tvBoValue = document.getElementById("list").value;
    var tvMeValue = document.getElementById("list").value;    
    var tvAnhValue = document.getElementById("list").value;   
    var tvEmValue = document.getElementById("list").value;
  
    if(tvBoValue){
        console.log("Xin chào", tvBoValue);
    }else if(tvMeValue){
        console.log("Xin chào", tvMeValue);
    }else if(tvAnhValue){
        console.log("Xin chào", tvAnhValue);
    }else{
        console.log("Xin chào", tvEmValue);
    }
});

// Bài 3

document.getElementById("btn-dem").addEventListener
("click",function(){
    var soMotValue = document.getElementById("txt-so-mot").value*1;
    var soHaiValue = document.getElementById("txt-so-hai").value*1;
    var soBaValue = document.getElementById("txt-so-ba").value*1;
    
    if(soMotValue%2==0 && soHaiValue%2 == 0 && 
        soBaValue%2 == 0){
            console.log("Có 3 số chẵn, 0 số lẻ");
    }else if(soMotValue % 2 == 0 && soHaiValue % 2 == 0 && soBaValue % 2 != 0){
        console.log("Có 2 số chẵn, 1 số lẻ");
    }else if(soMotValue % 2 == 0 && soHaiValue % 2 != 0 && soBaValue % 2 == 0){
        console.log("Có 2 số chẵn, 1 số lẻ");
    }else if(soMotValue % 2 != 0 && soHaiValue % 2 == 0 && soBaValue % 2 == 0){
        console.log("Có 2 số chẵn, 1 số lẻ");
    }else if(soMotValue % 2 != 0 && soHaiValue % 2 == 0 && soBaValue % 2 != 0){
        console.log("Có 1 số chẵn, 2 số lẻ");
    }else if(soMotValue % 2 != 0 && soHaiValue % 2 != 0 && soBaValue % 2 == 0){
        console.log("Có 1 số chẵn, 2 số lẻ");
    }else if(soMotValue % 2 == 0 && soHaiValue % 2 != 0 && soBaValue % 2 != 0){
        console.log("Có 1 số chẵn, 2 số lẻ");
    }else{
        console.log("Có 0 số chẵn, 3 số lẻ");
    }
});

// Bài 4 

document.getElementById("btn-du-doan").addEventListener
("click",function(){
    
    var canh1Value = document.getElementById("txt-canh-1").value*1;
    var canh2Value = document.getElementById("txt-canh-2").value*1;
    var canh3Value = document.getElementById("txt-canh-3").value*1;

    if(canh1Value == canh2Value && canh1Value == canh3Value && canh2Value == canh3Value){
        console.log("Hình Tam giác Đều");
    }else if(canh1Value==canh2Value || canh1Value==canh3Value || canh2Value==canh3Value){
        console.log("Hình Tam Giác Cân");
    }else if(canh1Value*canh1Value + canh2Value*canh2Value == canh3Value*canh3Value || 
        canh1Value*canh1Value + canh3Value*canh3Value == canh2Value*canh2Value ||
        canh2Value*canh2Value + canh3Value*canh3Value == canh1Value*canh1Value){
            console.log("Hình Tam Giác Vuông");
    }else{
        console.log("Tam Giác Khác");
    }
});